<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class updateguest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'guest_name' => 'required|string',
            'guest_ic' => 'required|string',
            'guest_pnum' => 'required|string',
            'guest_email' => 'required|string|unique:guest,email,'.Auth::guard('guest')->user()->guest_id.',guest_id',
            'password' => 'nullable|confirmed|min:6',
        ];
    }
}
