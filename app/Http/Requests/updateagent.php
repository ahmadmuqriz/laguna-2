<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class updateagent extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'agent_name' => 'required|string',
            'agent_ic' => 'required|string',
            'agent_pnum' => 'required|string',
            'agent_email' => 'required|string|unique:agents,agent_email,'.Auth::guard('agent')->user()->agent_id.',agent_id',
            'password' => 'nullable|confirmed|min:6',
        ];
    }
}
