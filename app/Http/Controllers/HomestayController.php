<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Homestay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomestayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homestays = Homestay::all();

//        $homestays = Homestay::paginate(5);
        return view('homestay.view', compact('homestays'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homestay.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return dd($request->all());

        $input = $request->validate([
            'house_name' => 'required',
            'house_type' => 'required',
            'house_num' => 'required',
            'house_own' => 'required',
            'house_pnum' => 'required | numeric',
            'room_count' => 'required',
            'house_rate' => 'required | numeric',
        ]);

        $image = $request->file('image');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $new_name);

        $image1 = $request->file('image1');
        $new_name1 = rand() . '.' . $image1->getClientOriginalExtension();
        $image1->move(public_path('images'), $new_name1);

        $image2 = $request->file('image2');
        $new_name2 = rand() . '.' . $image2->getClientOriginalExtension();
        $image2->move(public_path('images'), $new_name2);

        $image3 = $request->file('image3');
        $new_name3 = rand() . '.' . $image3->getClientOriginalExtension();
        $image3->move(public_path('images'), $new_name3);


        $homestays = Homestay::create([
            'agent_id' => Auth::guard('agent')->user()->agent_id,
            'house_name' => $input['house_name'],
            'house_type' => $input['house_type'],
            'house_num' => $input['house_num'],
            'house_own' => $input['house_own'],
            'house_pnum' => $input['house_pnum'],
            'room_count' => $input ['room_count'],
            'house_rate' => $input['house_rate'],
            'image' => $new_name,
            'image1' => $new_name1,
            'image2' => $new_name2,
            'image3' => $new_name3,
        ]);
        return redirect()->intended('/agent');

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Homestay  $homestay
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $homestays = Homestay::all();
        return view('homestay.customerview', compact('homestays'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Homestay  $homestay
     * @return \Illuminate\Http\Response
     */
    public function edit($house_id)
    {
//        $homestay= Homestay::where('house_id',$id)->first();

        $homestay = Homestay::findOrFail($house_id);
        return view('homestay.edit',compact('homestay'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Homestay  $homestay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $homestay = Homestay::where('house_id', $id)->first();

        if ($homestay) {

            $validatedData = $request->validate([
                'house_name' => 'required',
//                'house_type' =>'',
//                'house_num' => '',
                'house_own' => 'required',
                'house_pnum' => 'required | numeric',
                'room_count' => 'required',
                'house_rate' => 'required | numeric'
            ]);

            $homestay->update([
//                'house_id' => Auth::guard $validatedData('house')->user()->house_id,
                'house_name' => $validatedData['house_name'],
//               'house_type' => $validatedData['house_type'],
//               'house_num' => $validatedData['house_num'],
                'house_own' => $validatedData['house_own'],
                'house_pnum' => $validatedData['house_pnum'],
                'room_count' => $validatedData['room_count'],
                'house_rate' => $validatedData['house_rate']
            ]);
//            Homestay::whereId($house_id)->update($validatedData);

            return redirect('/homestay')->with('Yeay');
        } else {

            return redirect('/homestay')->with('Yeay');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Homestay  $homestay
     * @return \Illuminate\Http\Response
     */
    public function destroy($house_id)
    {
        $homestay = Homestay ::findOrFail($house_id);
        $homestay->delete();
        return redirect('/homestay')->with('success', 'Homestay Deleted');
    }
}
