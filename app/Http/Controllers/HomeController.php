<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\homestay;
//use App\Http\Requests\updatehomestay;
//use Illuminate\Http\Requests;
use Illuminate\support\facades\session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function store(Request $request)
    {
        return $request->get;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function agenthome()
    {
        return view('auth.agent.home');
    }

    public function guesthome()
    {
        return view('auth.guest.home');
    }
}
