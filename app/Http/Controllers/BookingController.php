<?php
namespace App\Http\Controllers;

use App\Homestay;
use App\Booking;
use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guest = Guest::where('guest_id', Auth::guard('guest')->user()->guest_id)->get();

        $booking = Booking::where('guest_id', Auth::guard('guest')->user()->guest_id)->simplePaginate(4);
        //return dd($bookings);
        return view('booking.index', compact('guest') ,compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

//    public function bookingcalc ()
//    {
//    }

    public function create ($id)
    {
//      $guests = Guest:: where('guest_id',$id)->first();
        $homestays = Homestay::where('house_id', $id)->first();

        return view('booking.create', compact ('homestays'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $input = $request->validate(
            [
                'checkin_date' => 'required|string',
                'checkout_date' => 'required|string',
                'booking_status' => 'required|string'
            ]);
        $booking = Booking::create(
            [
                'checkin_date' => $input['checkin_date'],
                'checkout_date' => $input['checkout_date'],
                'booking_status' => $input['booking_status'],
                'guest_id' => Auth::guard('guest')->user()->guest_id,
                'house_id' => $id,
            ]);

        return redirect('/booking');
    }

    public function show()
    {
    return view ('booking.create');
    }


    public function edit($id)
    {
        $bookings = Booking::where ('booking_id', $id)->first();

        return view( 'booking.update', compact('bookings'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $booking = booking:: where('booking_id',$id)->first();

    if($booking) {

        $input = $request->validate([

            'checkin_date' => 'required|string',
            'checkout_date' => 'required|string',
        ]);
        $booking->update([

            'checkin_date' => $input['checkin_date'],
            'checkout_date' => $input['checkout_date'],
        ]);
        return redirect(route('booking.index'));
    }

    else {
        return redirect()->back();
         }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function detail($id)
    {

            $bookings = Booking::where ('booking_id', $id)->first();
            $homestay = Homestay::where ('house_id', $id)->first();

        return view( 'auth.agent.agentdeletebook', compact('bookings'), compact('homestay'));

    }

    public function destroy($booking_id)
   {
        $booking = Booking::findorfail($booking_id);
        $booking -> delete();
        return redirect('/booking')->with('success','Booking deleted Successfuly');
    }

    public function destroyagent($booking_id)
    {
        $booking = Booking::findorfail($booking_id);
        $booking -> delete();
        return redirect('/agent')->with('success','Booking deleted Successfuly');
    }
}
