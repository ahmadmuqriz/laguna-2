<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Booking;
use App\Guest;
use App\Homestay;
use Illuminate\Http\Request;
use App\Http\Requests\updateguest;
//use Illuminate\Http\Requests;
use Illuminate\support\facades\session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guest = Guest::where('guest_id', Auth::guard('guest')->user()->guest_id)->get();

        $booking = Booking::where('guest_id', Auth::guard('guest')->user()->guest_id)->simplePaginate(4);

//        $booking = Booking::paginate(2);

        return view('guest.index',compact('guest'), compact('booking'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request->get;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guest= Guest::where('guest_id', $id)->first();


        return view('guest.viewguest', compact('guest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = Guest::where('guest_id', $id)->first();
        if ($guest->guest_id == Auth::user()->guest_id)
        {
            return view('guest.updateguest', compact('guest'));
        }
        else
        {
            return redirect('/guest/home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guest = Guest::where('guest_id', $id)->first();
        if ($guest){
            if (trim($request->password) == null)
            {
                $input = $request->except('password');
                $guest->update(
                    ['guest_name'=>$input['guest_name'],
                        'guest_ic'=>$input['guest_ic'],
                        'guest_pnum'=>$input['guest_pnum'],
                        'guest_email'=>$input['guest_email']
                    ]);
                Session::flash('update_profile','Update profile successfully');
                return redirect('/guest/'. $id);
            }
            else
            {
                $input = $request->all();
                $input['password'] = Hash::make($input['password']);
                $guest->update(
                    [ 'guest_name'=>$input['guest_name'],
                        'guest_ic'=>$input['guest_ic'],
                        'guest_pnum'=>$input['guest_pnum'],
                        'guest_email'=>$input['guest_email'],
                        'guest_pass'=>$input['password']
                    ]);
                Session::flash('update_profile','Update profile successfully');
                return redirect('/guest/'. $id);
            }
        }
        else
        {
            return redirect('/agent/home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($guest_id)
    {
        $guest = Guest ::findOrFail($guest_id);
        $guest->delete();
        return redirect('/guest/login')->with('success', 'Guest Account Deleted');
    }

}
