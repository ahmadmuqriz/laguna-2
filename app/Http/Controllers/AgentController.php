<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Booking;
use App\Homestay;
use Illuminate\Http\Request;
use App\Http\Requests\updateagent;
//use Illuminate\Http\Requests;
use Illuminate\support\facades\session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $booking = Booking::where('guest_id', Auth::guard('guest')->user()->guest_id)->get();

        $booking= Booking::all();

        $booking = Booking::paginate(5);
        return view('agent.index', compact('booking'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request->get;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent= Agent::where('agent_id', $id)->first();

        return view('auth.agent.viewagent', compact('agent'));
    }
    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::where('agent_id', $id)->first();
        if ($agent->agent_id == Auth::user()->agent_id)
        {
            return view('auth.agent.updateagent', compact('agent'));
        }
        else
        {
            return redirect('/agent/home');
        }
    }
    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateagent $request, $id)
    {
        $agent = Agent::where('agent_id', $id)->first();
        if ($agent){
            if (trim($request->password) == null)
            {
                $input = $request->except('password');
                $agent->update(
                    ['agent_name'=>$input['agent_name'],
                    'agent_ic'=>$input['agent_ic'],
                    'agent_pnum'=>$input['agent_pnum'],
                    'agent_email'=>$input['agent_email']
                    ]);
                Session::flash('update_profile','Update profile successfully');
                return redirect('/agent/'. $id);
            }
            else
            {
                $input = $request->all();
                $input['password'] = Hash::make($input['password']);
                $agent->update(
                   [ 'agent_name'=>$input['agent_name'],
                    'agent_ic'=>$input['agent_ic'],
                    'agent_pnum'=>$input['agent_pnum'],
                    'agent_email'=>$input['agent_email'],
                    'agent_pass'=>$input['password']
                 ]);
                Session::flash('update_profile','Update profile successfully');
                return redirect('/agent/'. $id);
            }
        }
        else
            {
            return redirect('/agent/home');
            }
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($agent_id)
    {
        $agent = Agent ::findOrFail($agent_id);
        $agent->delete();
        return redirect('/agent/login')->with('success', 'Agent Information Deleted');
    }
}
