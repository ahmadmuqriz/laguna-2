<?php

namespace App\Http\Controllers\Auth;

use App\Agent;
use App\Guest;
use App\homestay;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**Agent*/
    public function ShowRegisterAgent()
    {
        return view('auth.agent.register');
    }

    /**Guest*/
    public function ShowRegisterGuest()
    {
        return view('auth.guest.register');
    }
    /**homestay*/
    public function ShowRegisterHome()
    {
        return view('auth.agent.registerhome');
    }
    public function registerAgent(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'ic' => 'required',
            'pnum' => 'required',
            'email' => 'required|unique:agents,agent_email',
            'password' => 'required|confirmed',
        ]);

        $agent = Agent::create([
            'agent_name' => $input['name'],
            'agent_ic' => $input['ic'],
            'agent_pnum' => $input['pnum'],
            'agent_email' => $input['email'],
            'agent_pass' => Hash::make($input['password']),
        ]);

        return redirect()->intended('agent/login');
    }

    public function registerguest(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'ic' => 'required',
            'pnum' => 'required',
            'email' => 'required|unique:guests,guest_email',
            'password' => 'required|confirmed',
        ]);

        $agent = Guest::create([
            'guest_name' => $input['name'],
            'guest_ic' => $input['ic'],
            'guest_pnum' => $input['pnum'],
            'guest_email' => $input['email'],
            'guest_pass' => Hash::make($input['password']),
        ]);

        return redirect()->intended('guest/login');
    }

    public function registerhome(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'type' => 'required',
            'num' => 'required',
            'own' => 'required',
            'rate' => 'required|confirmed',
//            'price' => 'required',
        ]);

        $agent = homestay::create(
            [
            'house_name' => $input['name'],
            'house_type' => $input['type'],
            'house_num' => $input['num'],
            'house_own' => $input['own'],
            'house_pnum' => $input['pnum'],
            'house_rate' => $input['rate'],]);
//           'house_price' => $input['price'],
    }
}
