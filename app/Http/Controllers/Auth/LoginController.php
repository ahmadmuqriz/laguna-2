<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class LoginController extends Controller
{
    /*
    |-------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:agent')->except('logout');
        $this->middleware('guest:guest')->except('logout');
    }

    public function guestShowLogin()
    {
        return view('auth.guest.login');
    }

    public function agentShowLogin()
    {
        return view('auth.agent.login');
    }

    public function guestlogin(Request $request)
    {
      $request->validate([

          'email'=>'required|email',
          'password'=>'required|min:6',
      ]);

      if(Auth::guard('guest')->attempt(['guest_email'=> $request->email,
          'password' => $request->password])) {
          return redirect()->intended('/guest');
      }else
        return $this->sendFailedLoginResponse($request);
    }

    public function agentlogin(Request $request)
    {
        $request->validate([

            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);

        if(Auth::guard('agent')->attempt(['agent_email'=> $request->email, 'password' => $request->password])) {
            return redirect()->intended('/agent');
        }else
            return $this->sendFailedLoginResponse($request);
    }

}
