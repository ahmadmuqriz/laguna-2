<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agent extends Authenticatable
{
    protected $primaryKey = 'agent_id';

    protected $guard = 'agent';

    protected $table = 'agents';

    protected $guarded = [];


    public function getAuthPassword()
    {
        return $this->agent_pass;
    }
    protected $fillable =
       ['agent_id', 'agent_name','agent_ic','agent_pnum','agent_email','agent_pass'
    ];

}




