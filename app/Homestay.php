<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homestay extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'house_id';

    protected $fillable= [

        'house_id', 'house_name','house_type','house_num','house_own','house_pnum', 'room_count', 'house_rate', 'image','image1','image2','image3', 'agent_id',
    ];
}
