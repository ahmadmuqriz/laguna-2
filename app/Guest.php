<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Guest extends Authenticatable
{
    protected $primaryKey = 'guest_id';

    protected $guard = 'guest';

    protected $table = 'guests';

    protected $guarded = [];

    protected $fillable = [

        'guest_id', 'guest_name', 'guest_ic', 'guest_pnum', 'guest_email', 'guest_pass'
        ];

    public function getAuthPassword()
    {
        return $this->guest_pass;
    }

}
