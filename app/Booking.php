<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Booking extends Model
{
    protected   $primaryKey ='booking_id';

    protected   $table ='bookings';

    protected   $guard ='bookings';

    protected   $guarded =[];

    protected   $fillable = [ 'booking_id', 'checkin_date', 'checkout_date','booking_status', 'guest_id', 'house_id',


    ];

    public function homestay()
    {
        return $this->belongsTo(Homestay::class,'house_id', 'house_id');
    }


    public function guest()
    {
        return $this->belongsTo(Guest::class,'guest_id', 'guest_id');
    }

}
