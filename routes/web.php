<?php
use Illuminate\Support\Facades\Route;
use App\Notifications\homestaybooked;
use App\agent;
use App\guest;



Route::get('/', function () {
    return view('homepage');
});

// Auth::routes();

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

//Agent
Route::get('/agent/login','Auth\LoginController@agentShowLogin')->name('agentLogin');

route::get('/agent/register','Auth\RegisterController@ShowRegisterAgent')->name('agentregister');

Route::get('/agent/edit/{id}', 'AgentController@edit')->name('editagent')->middleware('auth:agent');

Route::post('/agent/register','Auth\RegisterController@RegisterAgent')->name('registeragent');

Route::post('/agent/login','Auth\LoginController@agentlogin')->name('loginagent');

Route::put('/agent/update/{id}/update','AgentController@update')->name('updateagent');

Route::delete('/agent/{agent}', 'AgentController@destroy')->name('agent.destroy');


//Guest
Route::get('/guest/login','Auth\LoginController@guestShowLogin')->name('guestLogin');

route::get('/guest/register','Auth\RegisterController@ShowRegisterGuest')->name('guestregister');

Route::get('/guest/edit/{id}', 'GuestController@edit')->name('editguest')->middleware('auth:guest');

Route::get('guest/view/{id}', 'GuestController@show')->name('guest.show')->middleware('auth:guest');

Route::post('/guest/register','Auth\RegisterController@registerguest')->name('registerguest');

Route::post('/guest/login','Auth\LoginController@guestlogin')->name('loginguest');

Route::put('/guest/update/{id}/update','GuestController@update')->name('updateguest');

Route::delete('/guest/{guest}', 'GuestController@destroy')->name('guest.destroy');



// handle agent and guest

Route::resource('/agent', 'AgentController')->middleware('auth:agent');

Route::resource('/guest', 'GuestController')->middleware('auth:guest');

//homestay

Route::resource('/homestay', 'HomestayController');


//Route::resource('/booking', 'BookingController', ['except' => ['create'] ])->middleware('auth:guest', 'auth:agent');

route::get('/homestay/view', 'HomestayController@show')->name('homestay.view')->middleware('auth:guest');
////
//route::post('/homestay/register', 'HomestayController@store')->name('homestay.store');

//Booking
//Route::get('/booking/create', 'BookingController@create')->middleware('auth:guest')->name('booking.homestay');

route::get('/booking/create/{id}', 'BookingController@create')->name('booking.create')->middleware('auth:guest');

route::post('/booking/store/{id}', 'BookingController@store')->name('booking.store');

route::get('/booking', 'BookingController@index')->name('booking.index')->middleware('auth:guest');

route::delete('/booking/delete/{booking_id}', 'BookingController@destroy')->name('booking.destroy')->middleware('auth:guest');

route::delete('/booking/deleteagent/{booking_id}', 'BookingController@destroyagent')->name('booking.destroyagent')->middleware('auth:agent');


route::get('/booking/detail/{id}', 'BookingController@detail')->name('booking.detail');




//must have 2 unit edit & update
route::get('/booking/edit/{id}', 'BookingController@edit')->name('booking.edit')->middleware('auth:guest');

route::post('/booking/update/{id}','BookingController@update')->name('booking.update');

//
//Route::get('/homestay/{homestay}/edit', 'HomestayController@edit')->name('homestay.edit');
//
//Route::patch('/homestay/{id}', 'HomestayController@update')->name('homestay.update');
//
//Route::delete('/booking/{}', 'HomestayController@destroy')->name('homestay.destroy');

//route::get('/homestay/register', 'HomestayController@registerhome')->name('registerhome');
