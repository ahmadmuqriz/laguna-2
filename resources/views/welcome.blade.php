<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome To Bandar Laguna Merbok Homestay</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('smolicon.ico') }}" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Segoe UI', bold ,sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                @if(Auth::guard('agent')->check())
                    <a href="{{ route('agent.index') }}">Home</a>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @elseif(Auth::guard('guest')->check())
                    <a href="{{ route('guest.index') }}">Home</a>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <a class="nav-link" href="{{ route('agentLogin') }}">{{ __('Agent Login') }}</a>
                    <a class="nav-link" href="{{ route('guestLogin') }}">{{ __('COACH Login') }}</a>
                @endif
            </div>

            <div class="content">
                <div class="title m-b-md">
                    Laguna Merbok Homestay Booking System
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Homestays</a>
                    <a href="https://laracasts.com">Cross Country</a>
                    <a href="https://laravel-news.com">Half Marathon</a>
                    <a href="https://blog.laravel.com">Full Marathon</a>
                    <a href="https://nova.laravel.com">Coaching</a>
                    <a href="https://forge.laravel.com">Speed</a>
                    <a href="https://github.com/laravel/laravel">Personal Best</a>
                </div>
            </div>
        </div>
    </body>
</html>
