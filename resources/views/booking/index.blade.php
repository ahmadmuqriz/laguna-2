@extends('layouts.adminmainguest')
@section('content')
    {{--    <div class="row">--}}
    {{--        <!-- ============================================================== -->--}}
    <!-- basic table  -->
    <!-- ============================================================== -->
    <div class="col-xl-03 col-lg-03 col-md-03 col-sm-01 col-12">
        <div class="card">
            <h3 class="card-header">Guest Booking List</h3>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered first">
                        <thead>
                        <tr>
                            <center>
                                <th>Booking ID</th>
                                <th>Homestay Image</th>
                                <th>House Name</th>
                                <th>House Type</th>
                                <th>House Phone Number</th>
                                <th>House Address</th>
                                <th>Check In Date</th>
                                <th>Check Out Date</th>
                                <th>Booking Status</th>
{{--                                <th>Booking Rate per Night (RM)</th>--}}
                                <th>Action</th>
                        </tr>
                        </thead>
                        @foreach($booking as $bookings)
                            <tr>
                                <td>{{$bookings->booking_id}}</td>
                                <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image }}" class="img-thumbnail" width="100" /></td>

                                <td>{{$bookings->homestay->house_name}}</td>
                                <td>{{$bookings->homestay->house_type}}</td>
                                <td>{{$bookings->homestay->house_pnum}}</td>
                                <td>{{$bookings->homestay->house_num}}</td>
                                <td>{{$bookings->checkin_date}}</td>
                                <td>{{$bookings->checkout_date}}</td>
                                <td>{{$bookings->booking_status}}</td>
{{--                                <td> {{$bookings->booking_rate }}</td>--}}
                                <td>
                                    <ul>
                                        <a class= "btn btn-rounded btn-primary btn-md" href="{{route('booking.edit',$bookings->booking_id)}}">Update</a>
                                    </ul>
{{--                                    <ul>--}}
{{--                                        <a class= "btn btn-rounded btn-success" href="" >View</a>--}}
{{--                                    </ul>--}}
                                    <form action ="{{ route ('booking.destroy', $bookings->booking_id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <ul><button class ="btn btn-rounded btn-secondary" type="submit" padding: 5px; href="route ('booking.destroy', $->house_id)">Delete</button></ul>
                                    </form>
                                </td>
                            </tr>
                            </form>
                            </a>
                            </td>
                            </tr>
                        @endforeach
                    </table><BR>
                    {{ $booking->links()}}

                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->
    {{--    </div>--}}
@endsection
