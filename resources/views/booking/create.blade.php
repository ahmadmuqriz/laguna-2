@extends('layouts.adminmainguest')
@section('content')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .zoomin img {
            height: 200px;
            width: 200px;
            -webkit-transition: all 2s ease;
            -moz-transition: all 2s ease;
            -ms-transition: all 2s ease;
            transition: all 2s ease;
        }
        .zoomin img:hover {
            width:310px;
            height:310px;
        }
    </style>

        <div class="row justify-content-center">
            <div class="col-md-11">
                <BR>
                <br>
                <div class="card">
                    <H3><B><div class="card-header">{{ __('Booking Review') }}</div></B></H3>
        <div class="card-body">

                    <center><h3><td>The {{$homestays->house_name}}</td></h3></center>
            <center>
                <div class="zoomin">
                    <td><img src="{{ URL::to('/') }}/images/{{ $homestays->image }}" class="img-thumbnail" width="300" /></td>
                    <td><img src="{{ URL::to('/') }}/images/{{ $homestays->image1 }}" class="img-thumbnail" width="300" /></td>
                    <td><img src="{{ URL::to('/') }}/images/{{ $homestays->image2 }}" class="img-thumbnail" width="300" /></td>
                    <td><img src="{{ URL::to('/') }}/images/{{ $homestays->image3 }}" class="img-thumbnail" width="300" /></td>
                </div>
            </center>
            <hr>
                    <form method="POST" action="{{route('booking.store',$homestays->house_id)}}">
            @csrf
                        <br>

{{--                          <div class="form-group row">--}}
                              <center>
                                  <div class="col-xl-4">
                                      <label>Check In Date :</label>
                                  </div>
                                  <input id="datetimepicker" width="312" name="checkin_date" class="form-control @error('checkin_date') is-invalid @enderror" value="{{ old('checkin_date')}}">
                                  <script>
                                      $('#datetimepicker').datetimepicker({format: 'yyyy-mm-dd',  datepicker: {
                                              disableDates:  function (date) {
                                                  const currentDate = new Date();
                                                  return date > currentDate ? true : false;
                                              }
                                          }});
                                  </script>
                                  @error('checkin_date')
                                  <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                              @enderror
                              </center>
                        <br>

                        <div class="form-group"><center>
                                <div class="col-xl-4">
                                    <label>Check Out Date :</label>
                                </div>
                                <input id="datetimepicker2" width="312" name="checkout_date" class="form-control @error('checkout_date') is-invalid @enderror" value="{{ old('checkout_date')}}">

                                <script>
                                    $('#datetimepicker2').datetimepicker({ format: 'yyyy-mm-dd',  datepicker: {
                                            disableDates:  function (date) {
                                                const currentDate = new Date();
                                                return date > currentDate ? true : false;
                                            }
                                        }});
                                </script>
                                @error('checkout_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div></center><br>


                <div class="form-group row">
                    <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('House Address :') }}</label>
                    <div class="col-md-7">
                        <label for="#" class="col-md-0 col-form-label text-md-left">{{($homestays->house_num) }}</label>
{{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                    </div>
                </div>


                <div class="form-group row">
                    <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('House Type :') }}</label>
                    <div class="col-md-7">
                            <label for="#" class="col-md-0 col-form-label text-md-right">{{($homestays->house_type)}}</label>
                            <input id="" type="hidden"  style="width:160px;" disabled name="house_type" value="{{($homestays->house_type)}}">
{{--                        @error('booking_rate')--}}

                    </div>
                </div>

                        <div class="form-group row">
                            <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('Number of Room : ') }}</label>
                            <div class="col-md-7">
                                <label for="#" class="col-md-0 col-form-label text-md-right">{{($homestays->room_count)}}</label>
                                <input id="" type="hidden"  style="width:160px;"  disabled name="house_type" value="{{($homestays->room_count)}}">
                            </div>
                        </div>


{{--                <div class="form-group row">--}}
{{--                    <label for="booking_rate" class="col-md-4 col-form-label text-md-right">{{ __('Rate Per Night (RM) : ') }}</label>--}}
{{--                    <div class="col-md-2">--}}
{{--                        <input id="" type="text" style="width:160px;" class="form-control @error('booking_rate') is-invalid @enderror" name="booking_rate" value="{{($homestays->house_rate)}}" >--}}
{{--                        @error('booking_rate')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}


                        <div class="form-group row">
                            <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('Rate Per Night (RM) : ') }}</label>
                            <div class="col-md-7">
                                <label for="#" class="col-md-0 col-form-label text-md-right">{{($homestays->house_rate) }}</label>
                                {{--                                <input id="" type="hidden"  style="width:160px;"  disabled name="house_type" value="{{($homestays->room_count)}}">--}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="booking_status" class="col-md-4 col-form-label text-md-right">{{ __('Booking Confirmation :') }}</label>
                            <div class="col-md-6">
                                <select name="booking_status" width="40">
                                    <option value="" disabled selected>Please Select</option>
                                    <option value="Booked">Agree</option>
                                </select>
                                @error('booking_status')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror<br><br>
                                <span><b>Disclaimer:</b> <p align="justify"> This is to confirm that you agree upon booking the house in which all responsibility are limited to the guest staying period. This includes all guest belongings and properties
                                    in which owner and agent of the homestay solely not responsible. </p></span>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-5">
                        <button type="submit" class="btn btn-rounded btn-brand">
                            {{ __('Book Now')}}
                        </button>
                    </div>
                </div>

            </form>
            </div>
                </div>
            </div>
        </div>

    <script>
        // $('#datetimepicker').datepicker();
        // $('#datetimepicker2').datepicker();
    </script>

@endsection
