@extends('layouts.adminmainguest')
@section('content')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .zoomin img {
            height: 200px;
            width: 200px;
            -webkit-transition: all 2s ease;
            -moz-transition: all 2s ease;
            -ms-transition: all 2s ease;
            transition: all 2s ease;
        }
        .zoomin img:hover {
            width:310px;
            height:310px;
        }
    </style>


    <div class="row justify-content-center">
        <div class="col-md-11">
            <BR>
            <br>
            <div class="card">
                <h3><B><div class="card-header">{{ __('Update Booking Review') }}</div></B></h3>
                <div class="card-body">

                    <center><h1><td>The {{$bookings->homestay->house_name}}</td></h1></center>
                    <div class = "zoomin">
                    <center>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image}}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image1}}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image2}}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image3}}" class="img-thumbnail" width="300" /></td>
                    </center></div>

                    <form method="POST" action="{{route('booking.update',$bookings->booking_id)}}">
                        @csrf
<hr>
                        <center>
                            <div class="col-xl-4">
                                <label>Check In Date :</label>
                            </div>
                            <input id="datetimepicker" width="312" name="checkin_date" class="form-control @error('checkin_date') is-invalid @enderror" value="{{ old('checkin_date')}}">
                            <script>
                                $('#datetimepicker').datetimepicker({format: 'yyyy-mm-dd'  });
                            </script>
                            @error('checkin_date')
                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                            @enderror
                        </center>
                        <br>

                        <div class="form-group"><center>
                                <div class="col-xl-4">
                                    <label>Check Out Date :</label>
                                </div>
                                <input id="datetimepicker2" width="312" name="checkout_date" class="form-control @error('checkout_date') is-invalid @enderror" value="{{ old('checkout_date')}}">

                                <script>
                                    $('#datetimepicker2').datetimepicker({ format: 'yyyy-mm-dd'  });
                                </script>
                                @error('checkout_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div></center><br>

                    <div class="row">
                        <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('House Address:') }}</label>
                        <div class="col-md-7">
                            <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->homestay->house_num) }}</label>
                            {{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                        </div>
                    </div>


                        <div class="form-group row">
                            <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('House Type :') }}</label>
                            <div class="col-md-7">
                                <label for="#" class="col-md-0 col-form-label text-md-right">{{($bookings->homestay->house_type)}}</label>
{{--                                <input id="" type="hidden"  style="width:160px;" disabled name="house_type" value="{{({{($bookings->homestay->house_num) }}z)}}">--}}
                                {{--                        @error('booking_rate')--}}

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('Number of Room : ') }}</label>
                            <div class="col-md-7">
                                <label for="#" class="col-md-0 col-form-label text-md-right">{{($bookings->homestay->room_count) }}</label>
{{--                                <input id="" type="hidden"  style="width:160px;"  disabled name="house_type" value="{{($homestays->room_count)}}">--}}
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="#" class="col-md-4 col-form-label text-md-right">{{ __('Rate Per Night (RM) : ') }}</label>
                            <div class="col-md-7">
                                <label for="#" class="col-md-0 col-form-label text-md-right">{{($bookings->homestay->house_rate) }}</label>
                                {{--                                <input id="" type="hidden"  style="width:160px;"  disabled name="house_type" value="{{($homestays->room_count)}}">--}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-5">
                            <button type="submit" class="btn btn-rounded btn-brand">
                                {{ __('Update Booking')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>

    <script>
        // $('#datetimepicker').datepicker();
        // $('#datetimepicker2').datepicker();
    </script>

@endsection
