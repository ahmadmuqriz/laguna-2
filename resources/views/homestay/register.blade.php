@extends('layouts.adminmain')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <BR>
                <br>
                <div class="card">
                    <B><center><h3><div class="card-header">{{ __('Register Homestay') }}</div></h3></center></B>

                    <div class="card-body">
{{--                            <label for="ImageHomestay Image" class="col-md-4 col-form-label text-md-right">{{ __('House Name') }}</label>--}}
{{--                            <input type="file" id="Image" class="form-control @error('title') is-invalid @enderror" name="image" value="{{ isset($product) ? $product -> image :''}}">--}}
{{--                            @error('image')--}}
{{--                            <strong class="alert alert-danger">{{ $message }}</strong>--}}
{{--                            @enderror--}}
                        <form method="POST" action="{{route('homestay.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="house_name" class="col-md-4 col-form-label text-md-right">{{ __('House Name:') }}</label>
                                <div class="col-md-6">
                                    <input id="house_name" type="text" class="form-control @error('house_name') is-invalid @enderror" name="house_name" value="{{ old('house_name') }}" required autocomplete="house_name">
                                    @error('house_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="house_type" class="col-md-4 col-form-label text-md-right">{{ __('House Type:')}}</label>
                                <div class="col-md-6">
                                    <select name="house_type">
                                        <option value="" disabled selected>Please Select</option>
                                        <option value="2 Storey Terrace">2 Storey Terrace</option>
                                        <option value="Semi-D"> Semi-D </option>
                                        <option value="Bungalow"> Bungalow </option>
                                        <option value="Corner Lot"> Corner Lot </option>
                                    </select>
{{--                                    <input id="house_type" type="text" class="form-control @error('house_type') is-invalid @enderror" name="house_type" value="{{ old('house_type')}}" required autocomplete="house_type">--}}
                                    @error('house_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="house_num" class="col-md-4 col-form-label text-md-right">{{ __('House Address:')}}</label>
                                <div class="col-md-6">
                                    <input id="house_num" type="text"  class="form-control @error('house_num') is-invalid @enderror" name="house_num" value="{{old('house_num')}}" required autocomplete="house_num" autofocus>
                                    @error('house_num')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_own" class="col-md-4 col-form-label text-md-right">{{ __('House Owner:') }}</label>
                                <div class="col-md-6">
                                    <input id="house_own" type="text" class="form-control @error('house_own') is-invalid @enderror" name="house_own" value="{{ old('house_own') }}" required autocomplete="house_own">
                                    @error('house_own')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_pnum" class="col-md-4 col-form-label text-md-right">{{ __('House Phone Number:')}}</label>
                                <div class="col-md-6">
                                    <input id="house_pnum" type="number" class="form-control @error('house_pnum') is-invalid @enderror" name="house_pnum" max="99999999999" value="{{ old('house_pnum') }}" required autocomplete="house_pnum">
                                    <span>Format: 01234567890</span>
                                    @error('house_pnum')

                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="room_count" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Room:')}}</label>
                                <div class="col-md-6">
                                    <select name="room_count">
                                        <option value="" disabled selected>Please Select</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    @error('room_count')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_rate" class="col-md-4 col-form-label text-md-right">{{ __('House Rate (RM):')}}</label>
                                <div class="col-md-6">
                                    <input id="house_rate" type="number" class="form-control" @error("house_rate") is-invalid @enderror name="house_rate" min="100" max="999" required autocomplete="house_rate">
                                    @error('house_rate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>

                            <center>
                                <label> Homestay Image 1 : </label>
                                <div class="col-md-6">
                                    <input type="file" name="image"  class="form-control @error('image') is-invalid @enderror" value="{{ old('image') }}">
                                </div>
                            </center><br>

                            <center>
                                <label> Homestay Image 2 : </label>
                                <div class="col-md-6">
                                    <input type="file" name="image1"  class="form-control @error('image1') is-invalid @enderror" value="{{ old('image1') }}">
                                </div>
                            </center><BR>

                            <center>
                                <label> Homestay Image 3: </label>
                                <div class="col-md-6">
                                    <input type="file" name="image2"  class="form-control @error('image2') is-invalid @enderror" value="{{ old('image2') }}">
                                </div>
                            </center><BR>

                            <center>
                                <label> Homestay Image 4 : </label>
                                <div class="col-md-6">
                                    <input type="file" name="image3"  class="form-control @error('image3') is-invalid @enderror" value="{{ old('image3') }}">
                                </div>
                            </center><BR>

                            {{--                            <div class="form-group row">--}}
{{--                                <label class="col col-md-3"> Homestay Image</label>--}}
{{--                                <div class="col-12 col-md-9">--}}
{{--                                    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') }}">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group row">--}}
{{--                                <label class="col col-md-3"> Homestay Image 2</label>--}}
{{--                                <div class="col-12 col-md-9">--}}
{{--                                    <input type="file" name="image1" class="form-control @error('image1') is-invalid @enderror" value="{{ old('image1') }}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group row">--}}
{{--                                <label class="col col-md-3"> Homestay Image 3</label>--}}
{{--                                <div class="col-12 col-md-9">--}}
{{--                                    <input type="file" name="image2" class="form-control @error('image2') is-invalid @enderror" value="{{ old('image2') }}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group row">--}}
{{--                                <label class="col col-md-3"> Homestay Image 4</label>--}}
{{--                                <div class="col-12 col-md-9">--}}
{{--                                    <input type="file" name="image3" class="form-control @error('image3') is-invalid @enderror" value="{{ old('image3') }}">--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-rounded btn-brand">
                                        {{ __('Register Homestay')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
