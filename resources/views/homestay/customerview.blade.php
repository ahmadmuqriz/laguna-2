@extends('layouts.adminmainguest')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

@section('content')
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
    @endif



    {{--<div class="row">--}}
    {{--<!-- ============================================================== -->--}}
    <!-- basic table  -->
    <!-- ============================================================== -->


             <div class="card-header">

            <h3 class="card-header">Homestay List</h3><BR><BR>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered first">
                        <thead>
                        <tr>
                            <th>Homestay Image
                            <th>House ID</th>
                            <th>House Name</th>
                            <th>House Type</th>
                            <th>House Address</th>
                            <th>House Owner</th>
                            <th>House Phone Number</th>
                            <th>House Rate</th>
                            <center></center><th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($homestays as $homestay)
                            <tr>
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image }}" class="img-thumbnail" width="100" /></td>
                                <td>{{$homestay->house_id}}</td>
                                <td>{{$homestay->house_name}}</td>
                                <td>{{$homestay->house_type}}</td>
                                <td>{{$homestay->house_num}}</td>
                                <td>{{$homestay->house_own}}</td>
                                <td>{{$homestay->house_pnum}}</td>
                                <td>{{$homestay->house_rate}}</td>
                                <td>

{{--                                        <a class= "btn btn-rounded btn-primary btn-md" href="{{ route('booking.create', Auth::guard('guest')->user()->guest_id) }}}}" >Book Now</a>--}}
{{--                                        --}}

                                        <a button class ="btn btn-success" type="submit" padding: 8px; href="{{ route('booking.create',$homestay->house_id)}}">Book

                                    {{--                                    <form action ="{{ route ('homestay.destroy', $homestay->house_id)}}" method="post">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <ul><button class ="btn btn-rounded btn-secondary" type="submit" padding: 5px; href="route ('homestay.destroy', $homestay->house_id)">Delete</button></ul>--}}
{{--                                    </form>--}}
                                </td>
                            </tr>
                        </tbody>

                            </a>
                            </td>
                            </tr>
                        @endforeach
                    </table>
{{--                    {{ $homestays->links()}}--}}
                </div>
            </div>
        </CENTER>

    <!-- ============================================================== -->
    <!-- end basic table  -->
    <!-- ============================================================== -->
    {{--    </div>--}}
    <script>
        $('#datepicker').datepicker();
        $('#datepicker2').datepicker();
    </script>
@endsection
