@extends('layouts.adminmain')
@section('content')

    <style type="text/css">
        .zoomin img {
            height: 200px;
            width: 200px;
            -webkit-transition: all 2s ease;
            -moz-transition: all 2s ease;
            -ms-transition: all 2s ease;
            transition: all 2s ease;
        }
        .zoomin img:hover {
            width:310px;
            height:310px;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <BR>
                <br>
                <div class="card">
                    <center><B><h3><div class="card-header">{{ __('Update Homestay') }}</div></h3></B></center>

                    <div class="card-body">
                        <form method="POST" action="{{route('homestay.update',$homestay->house_id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')

                            <center>
                             <div class="zoomin">
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image }}" class="img-thumbnail" width="300" /></td>
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image1 }}" class="img-thumbnail" width="300" /></td>
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image2 }}" class="img-thumbnail" width="300" /></td>
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image3 }}" class="img-thumbnail" width="300" /></td>
                             </div>
                            </center>
                            <hr>

                            <div class="form-group row ">
                                <label for="house_id" class="col-md-4 col-form-label text-md-right">{{ __('House ID :') }}</label>
                                <div class="col-md-6">
                                    <input id="house_id" type="text" class="form-control @error('house_id') is-invalid @enderror" name="house_name"  disabled value="{{$homestay->house_id}}" required autocomplete="house_id">
                                    @error('house_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_name" class="col-md-4 col-form-label text-md-right">{{ __('House Name :') }}</label>
                                <div class="col-md-6">
                                    <input id="house_name" type="text" class="form-control @error('house_name') is-invalid @enderror" name="house_name" value="{{$homestay->house_name}}" required autocomplete="house_name">
                                    @error('house_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="house_type" class="col-md-4 col-form-label text-md-right">{{ __('House Type :')}}</label>
                                <div class="col-md-6">
                                    <input id="house_type" type="text" class="form-control @error('house_type') is-invalid @enderror" name="house_type" disabled value="{{ $homestay->house_type }}" required autocomplete="house_type">
                                    @error('house_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="house_num" class="col-md-4 col-form-label text-md-right">{{ __('House Address :')}}</label>
                                <div class="col-md-6">
                                    <input id="house_num" type="text" class="form-control @error('house_num') is-invalid @enderror" name="house_num" disabled value="{{ $homestay->house_num}}" required autocomplete="house_num" autofocus>
                                    @error('house_num')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_own" class="col-md-4 col-form-label text-md-right">{{ __('House Owner :') }}</label>
                                <div class="col-md-6">
                                    <input id="house_own" type="text" class="form-control @error('house_own') is-invalid @enderror" name="house_own" value="{{$homestay->house_own}}" required autocomplete="house_own">
                                    @error('house_own')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_pnum" class="col-md-4 col-form-label text-md-right">{{ __('House Phone Number :')}}</label>
                                <div class="col-md-6">
                                    <input id="house_pnum" type="number" class="form-control @error('house_pnum') is-invalid @enderror" name="house_pnum" max="99999999999" value="{{$homestay->house_pnum}}" required autocomplete="house_pnum">
                                    <span>Format: 01234567890</span>
                                    @error('house_pnum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="room_count" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Room:')}}</label>
                                <div class="col-md-6">
                                    <input id="house_rate" type="number" class="form-control" @error("room_count") is-invalid @enderror name="room_count" min="0" max="999" value="{{$homestay->room_count}}" required autocomplete="room_count">
                                    @error('room_count')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_rate" class="col-md-4 col-form-label text-md-right">{{ __('House Rate (RM) :')}}</label>
                                <div class="col-md-6">
                                    <input id="house_rate" type="number" class="form-control" @error("house_rate") is-invalid @enderror name="house_rate" min="100" max="999" value="{{$homestay->house_rate}}" required autocomplete="house_rate">
                                    @error('house_rate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                    <hr>
                            <center>
                                        <label> Homestay Image 1 : </label>
                                        <div class="col-md-6" >
                                            <input type="file" name="image" width="300" class="form-control @error('image') is-invalid @enderror" value="{{$homestay->image}}">
                                        </div>
                                    </center><br>

                                    <center>
                                        <label> Homestay Image 2 : </label>
                                        <div class="col-md-6">
                                            <input type="file" name="image1"  class="form-control @error('image1') is-invalid @enderror" value="{{$homestay->image1}}">
                                        </div>
                                    </center><BR>

                                    <center>
                                        <label> Homestay Image 3: </label>
                                        <div class="col-md-6">
                                            <input type="file" name="image2"  class="form-control @error('image2') is-invalid @enderror" value="{{$homestay->image2}}">
                                        </div>
                                    </center><BR>

                                    <center>
                                        <label> Homestay Image 4 : </label>
                                        <div class="col-md-6">
                                            <input type="file" name="image3"  class="form-control @error('image3') is-invalid @enderror" value="{{$homestay->image3}}">
                                        </div>
                                    </center><BR>
<hr>
{{--                            <div class="form-group row mb-0">--}}
{{--                                <div class="col-md-6 offset-md-4">--}}
                                    <center><button type="submit" class="btn-rounded btn btn-primary" style="margin: 15px; padding: 15px; padding-right:30px; padding-left:30px">
                                        Update
                                        </button></center>
{{--                                    </form>--}}
                        </form>
                    </div>
                                    <form action ="{{ route ('homestay.destroy', $homestay->house_id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                       <center><button class ="btn-rounded btn btn-secondary" type="submit" style="margin: 5px; padding:15px; padding-right:20px; padding-left:20px";
                                            >Delete Homestay</button></center>
                                    </form>
{{--                                </div>--}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
