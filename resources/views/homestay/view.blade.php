@extends('layouts.adminmain')

@section('content')

        <div class="col-xl-03 col-lg-03 col-md-03 col-sm-01 col-12">
            <div class="card">
                <h5 class="card-header">Homestay List</h5>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <thead>
                            <tr>
                            <center>
                                <th>Homestay Image</th>
                                <th>House ID</th>
                                <th>House Name</th>
                                <th>House Type</th>
                                <th>House Address</th>
                                <th>House Owner</th>
                                <th>House Phone Number</th>
                                <th>Number of Room</th>
                                <th>House Rate</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            @foreach($homestays as $homestay)
                            <tr>
                                <td><img src="{{ URL::to('/') }}/images/{{ $homestay->image }}" class="img-thumbnail" width="100" /></td>
                                <td>{{$homestay->house_id}}</td>
                                <td>{{$homestay->house_name}}</td>
                                <td>{{$homestay->house_type}}</td>
                                <td>{{$homestay->house_num}}</td>
                                <td>{{$homestay->house_own}}</td>
                                <td>{{$homestay->house_pnum}}</td>
                                <td>{{$homestay->room_count}}</td>
                                <td>{{$homestay->house_rate}}</td>
                                <td>
                                    <ul>
                                            <a class= "btn btn-rounded btn-primary btn-md" href="{{ route('homestay.edit',$homestay->house_id)}}" >Update </a>
                                    </ul>
                                    <form action ="{{ route ('homestay.destroy', $homestay->house_id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    <ul><button class ="btn btn-rounded btn-secondary" type="submit" padding: 5px; href="route ('homestay.destroy', $homestay->house_id)">Delete</button></ul>
                                    </form>
                                </td>
                            </tr>
                                    </form>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
@endsection
