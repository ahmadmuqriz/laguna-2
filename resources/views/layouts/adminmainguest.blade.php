<!doctype html>
<html lang="en">


<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('smolicon.ico') }}" />

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href ="../../template/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../../template/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../template/assets/libs/css/style.css">
    <link rel="stylesheet" href="../../template/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="../../template/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="../../template/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="../../template/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../template/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="../../template/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Welcome To Bandar Laguna Merbok Homestay</title>
</head>

<body>
<!-- ============================================================== -->
<!-- main wrapper -->
<!-- ============================================================== -->
<div class="dashboard-main-wrapper">
    <!-- ============================================================== -->
    <!-- navbar -->
    <!-- ============================================================== -->
    <div class="dashboard-header">
        <nav class="navbar navbar-expand-lg bg-primary fixed-top">
            <a class="navbar-brand1" href="{{route('guest.index')}}">BANDAR LAGUNA MERBOK CENTRALIZED HOMESTAY BOOKING SYSTEM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-right-top">
                    <li class="nav-item dropdown notification">
                        {{-- <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> </a>--}}
                        <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                            <li>
                                <div class="notification-title"> Notification</div>
                                <div class="notification-list">
                                </div>
                            </li>
                            <li>
                                <div class="list-footer"> <a href="#">View all notifications</a></div>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown connection">
                        <a class="nav-link" href="{{route('guest.index')}}"> <i class=" fas fa-home"></i> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </li>
                    <li>
                        <ul class="dropdown-menu dropdown-menu-right connection-dropdown"></ul>
                    </li>
                    <li class="nav-item dropdown nav-user">
                        <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/" alt="" class="fas fa-user"></a>
                        <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                            <div class="nav-user-info">
                                <h5 class="mb-0 text-white nav-user-name">Haii {{Auth::guard('guest')->user()->guest_name}}</h5>
                                <span class="status"></span><span class="ml-2">{{Auth::guard('guest')->user()->guest_email}}</span>
                            </div>
                            <a class="dropdown-item" href="{{ route('guest.show', Auth::guard('guest')->user()->guest_id) }}"><i class="fas fa-user mr-2"></i>View Account</a>
                            <a class="dropdown-item" href="{{ route('editguest', Auth::guard('guest')->user()->guest_id)}}"><i class="fas fa-cog mr-2"></i>Account Setting</a>
                            <a class="dropdown-item" href=" {{ route('logout') }}"
                               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- ============================================================== -->
    <!-- end navbar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- left sidebar -->
    <!-- ============================================================== -->
    <div class="nav-left-sidebar sidebar-dark1">
        <div class="menu-list">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav flex-column">
                        <li class="nav-divider">
                            <center> <h4>Laguna Merbok Homestay Management System</h4></center>
                            _____________________________<br>
                            <br>
                            Guest Dashboard
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fas fa-book"></i>Booking<span class="badge badge-success"></span></a>
                            <div id="submenu-1" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">

                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('booking.index')}}">View Booking</a>
                                    </li>
                                    <li class="nav-item">
{{--                                        <a class="nav-link" href="#">Update Booking</a>--}}
                                    </li>
                                     <li class="nav-item">
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fas fa-home"></i>Homestay</a>
                            <div id="submenu-2" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('homestay.view') }}">View Homestay<span class="badge badge-secondary"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-address-book"></i>Guest Profile</a>
                            <div id="submenu-3" class="collapse submenu" style="">
                                <ul class="nav flex-column">
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href=" {{route('view',Auth::guard()->user()->agent_id)}}">View Profile</a>--}}
{{--                                    </li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('guest.show', Auth::guard('guest')->user()->guest_id) }}">View Profile</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('editguest', Auth::guard('guest')->user()->guest_id)}}">Update Profile</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end left sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @yield('content')
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<!-- jquery 3.3.1 -->
<script src="../../template/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- bootstap bundle js -->
<script src="../../template/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<!-- slimscroll js -->
<script src="../../template/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<!-- main js -->
<script src="../../template/assets/libs/js/main-js.js"></script>
<!-- chart chartist js -->
<script src="../../template/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
<!-- sparkline js -->
<script src="../../template/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
<!-- morris js -->
<script src="../../template/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
<script src="../../template/assets/vendor/charts/morris-bundle/morris.js"></script>
<!-- chart c3 js -->
<script src="../../template/assets/vendor/charts/c3charts/c3.min.js"></script>
<script src="../../template/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
<script src="../../template/assets/vendor/charts/c3charts/C3chartjs.js"></script>
<script src="../../template/assets/libs/js/dashboard-ecommerce.js"></script>
</body>

</html>
