
@extends('layouts.adminmain')
@section('content')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        .zoomin img {
            height: 200px;
            width: 200px;
            -webkit-transition: all 2s ease;
            -moz-transition: all 2s ease;
            -ms-transition: all 2s ease;
            transition: all 2s ease;
        }
        .zoomin img:hover {
            width:310px;
            height:310px;
        }
    </style>


    <div class="row justify-content-center">
        <div class="col-md-11">
            <BR>
            <br>
            <div class="card">
                <h3><B><div class="card-header">{{ __('Booking Detail') }}</div></B></h3>
                {{--        <div class="card-body">--}}

                    <center><h1><td>The {{$bookings->homestay->house_name}}</td></h1></center>

                <center>
                    <div class="zoomin">
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image }}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image1 }}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image2 }}" class="img-thumbnail" width="300" /></td>
                        <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image3 }}" class="img-thumbnail" width="300" /></td>
                    </div>
                </center>
                <hr>


                <div class="row">
                    <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Booking ID Number') }}</label>
                    <div class="col-md-7">
                        <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->booking_id) }}</label>
                        {{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                    </div>
                </div>

                    <div class="row">
                        <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Guest Name:') }}</label>
                        <div class="col-md-7">
                            <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->guest->guest_name) }}</label>
                            {{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                        </div>
                    </div>

                <div class="row">
                    <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Guest Phone Number: ') }}</label>
                    <div class="col-md-7">
                        <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->guest->guest_pnum) }}</label>
                        {{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                    </div>
                </div>

                <div class="row">
                    <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Booked Homestay Name: ') }}</label>
                    <div class="col-md-7">
                        <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->homestay->house_name) }}</label>
                        {{--                        <input id="house_num" style="height:90px; width:365px;" disabled name="house_num" value="{{($homestays->house_num)}}">--}}
                    </div>
                </div>

                    <div class="group row">
                        <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Homestay  Type:') }}</label>
                        <div class="col-md-7">
                            <label for="#" class="col-md-0 col-form-label text-md-left">{{($bookings->homestay->house_type) }}</label>
                        </div>
                    </div><BR>

                <div class="group row">
                    <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Check In Date:')}}</label>
                    <div class="col-md-7">
                        <input id="" type="text"  style="width:160px;"  disabled name="house_type"  value="{{($bookings->checkin_date)}}" ><br>
                        YY-MM-DD
                    </div>
                </div>

                <div class="group row">
                    <label for="#" class="col-md-5 col-form-label text-md-right">{{ __('Check Out Date:')}}</label>
                    <div class="col-md-7">
                        <input id="" type="text"  style="width:160px;"  disabled name="house_type"  value="{{($bookings->checkout_date)}}"><br>
                        YY-MM-DD
                    </div>
                </div><br>

                    <div class="form-group row">
                        <label for="booking_rate" class="col-md-5 col-form-label text-md-right">{{ __('Rate Per Night (RM): ') }}</label>
                        <div class="col-md-2">
                            <input id="booking_rate" type="text" style="width:160px;"  class="form-control @error('booking_rate') is-invalid @enderror" name="booking_rate" disabled value="{{($bookings->homestay->house_rate)}}" >
                            @error('booking_rate')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                <hr>
{{--                    <div class="form-group row mb-0">--}}
{{--                        <div class="col-md-7 offset-md-5">--}}
{{--                            <button type="submit" class="btn btn-rounded btn-danger">--}}
{{--                                {{ __('Delete Booking')}}--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                <div class="form-group row mb-0">
                    <div class="col-md-7 offset-md-5">
                    <form action ="{{ route ('booking.destroyagent', $bookings->booking_id)}}" method="post">
                    @csrf
                    @method('DELETE')
                        <button type="submit" class="btn btn-rounded btn-danger" type="submit" padding: 5px;>Delete</button>
                </form>
<br>
            </div>
        </div>
    </div>
    </div>

@endsection
