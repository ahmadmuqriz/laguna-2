@extends('layouts.adminmain')
@section('content')

    <br>
    <div class="container">
        <br><br>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <b><div class="card-header">{{ __('Agent Update') }}</div></b>

                    <div class="card-body">
                            <form class="cmxform form-horizontal style-form" id="signupForm" method="post" action="{{ route('updateagent', $agent->agent_id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                            <div class="form-group row">
                                <label for="agent_name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="agent_name" type="text" class="form-control @error('agent_name') is-invalid @enderror" name="agent_name" value="{{ $agent->agent_name }}" required autocomplete="agent_name" autofocus>

                                    @error('agent_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="agent_ic" class="col-md-4 col-form-label text-md-right">{{ __('IC Number') }}</label>

                                <div class="col-md-6">
                                    <input id="agent_ic" type="text" class="form-control @error('agent_ic') is-invalid @enderror" name="agent_ic" value="{{ $agent->agent_ic }}" required autocomplete="agent_ic" autofocus>

                                    @error('agent_ic')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pnum" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                <div class="col-md-6">
                                    <input id="agent_pnum" type="text" class="form-control @error('agent_pnum') is-invalid @enderror" name="agent_pnum" value="{{ $agent->agent_pnum }}" required autocomplete="agent_pnum" autofocus>

                                    @error('agent_pnum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="agent_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="agent_email" type="email" class="form-control @error('agent_email') is-invalid @enderror" name="agent_email" value="{{ $agent->agent_email }}" required autocomplete="agent_email" autofocus>

                                    @error('agent_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
