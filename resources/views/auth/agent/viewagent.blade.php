@extends('layouts.adminmain')

@section('content')
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
            text-style: bold;
        }
    </style>
    <div class="col-xl-03 col-lg-03 col-md-03 col-sm-01 col-12">
        <div class="card">
            <center>
                    <div class="card-body">
                        <b> <h3 class="card-title"> Agent Personal Information </h3></b><br>
                        <div class="table-responsive">
                            <table style="width:75%">
                                <tr>
                                    <th>Agent ID:</th>
                                    <td>{{ $agent->agent_id }}</td>
                                </tr>
                                <tr>
                                    <th>Agent Name:</th>
                                    <td>{{ $agent->agent_name }}</td>
                                </tr>
                                <tr>
                                    <th>Agent IC:</th>
                                    <td>{{ $agent->agent_ic }}</td>
                                </tr>
                                <tr>
                                    <th>Agent Phone Number:</th>
                                    <td>{{ $agent->agent_pnum }}</td>
                                </tr>
                                <tr>
                                    <th>Agent Email:</th>
                                    <td>{{ $agent->agent_email }}</td>
                                </tr>
                            </table>
                                <br><br>
                                        <a class= "btn btn-primary" href="{{ route('editagent', Auth::guard('agent')->user()->agent_id)}}">
                                            <span>Update Profile</span> </a>


                            <form action ="{{ route('agent.destroy', Auth::guard('agent')->user()->agent_id)}}" method="post">
                                @csrf
                                @method('DELETE')<br>
                                <button class ="btn btn-secondary" type="submit" padding: 10px; href="route('agent.destroy', Auth::guard('agent')->user()->agent_id))">Delete</button>
                            </form>
                            </td>
                            </tr>
                            </form>
{{--                                        <a class= "btn btn-secondary" href="">--}}
{{--                                            <span>Delete Profile</span> </a>--}}
                    </div>
            </center>
        </div>
    </div>
    </div>
    </div>
@endsection
