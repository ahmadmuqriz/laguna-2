@extends('layouts.app')

@section('content')

<!doctype html>
<html lang="en">

<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('smolicon.ico') }}" />

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Guest Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../template/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../../template/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../template/assets/libs/css/style.css">
    <link rel="stylesheet" href="../../template/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
        html,
        body {
            height: 100%;
        }
        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
        }
    </style>
</head>

<body>
<!-- ============================================================== -->
<!-- login page  -->
<!-- ============================================================== -->
<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center"><a href="#"><img class="logo-img" src="../../template/assets/images/logo.png" alt="logo" style="width:320px;height:120px;"> </a><span class="splash-description">Please Enter Guest Login Credential.</span></div>
        <div class="card-body">

            <form method="POST" action="{{route('loginguest')}}">
                    @csrf

                <div class="form-group">
{{--                    <input class="form-control form-control-lg" id="username" type="text" placeholder="Username" autocomplete="off">--}}
                    <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" placeholder="Guest Email" value="{{old('email')}}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
{{--                    <input class="form-control form-control-lg" id="password" type="password" placeholder="Password">--}}
                    <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Remember Me</span>
                    </label>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
            </form>
        </div>
        <hr>
{{--        <div class="card-footer bg-white p-0  ">--}}
            <div class="card-footer-item card-footer-item-bordered">
                <a href="{{ route('guestregister')}}" class="footer-link">Create An Account</a></div>
            <div class="card-footer-item card-footer-item-bordered">
        </div>
    </div>
</div>

<!-- ============================================================== -->
<!-- end login page  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="../../template/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="../../template/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>

</html>
@endsection
