@extends('layouts.adminmainguest')

@section('content')

{{--    <section class="wrapper site-min-height">--}}
{{--        <div class="row">--}}
{{--            <hr>--}}
{{--            <center> <div class="col-lg-10">--}}
{{--                <br>--}}
{{--                <div class="col-lg-20 col-md-20 col-sm-20 col-md-19">--}}
{{--                    <div class="list-group-item"></div>--}}
{{--                </div>--}}
{{--                <div class="card-body">--}}
{{--                    <h3 class="card-title"> Guest Personal Information </h3>--}}
{{--                    <p class="card-text">This is  Guest Personal Account Information which used to perform homestay booking .</p>--}}
{{--                </div>--}}
{{--                <ul class="list-group list-group-flush">--}}
{{--                    <li class="list-group-item"><b>Guest ID:</b> {{ $guest->guest_id }}</li>--}}
{{--                    <li class="list-group-item"><b>Guest Name:</b>  {{ $guest->guest_name }}</li>--}}
{{--                    <li class="list-group-item"><b>Guest IC:</b>  {{ $guest->guest_ic }}</li>--}}
{{--                    <li class="list-group-item"><b>Guest Phone Number:</b>  {{ $guest->guest_pnum }}</li>--}}
{{--                    <li class="list-group-item"><b>Guest Email:</b>  {{ $guest->guest_email }}</li>--}}

{{--                    <a class= "btn btn-primary" href="{{ route('editguest', Auth::guard('guest')->user()->guest_id)}}">--}}
{{--                        <span>Update Profile</span> </a>--}}
{{--                    <a class= "btn btn-secondary" href="">--}}
{{--                        <span>Delete Profile</span> </a>--}}
{{--                    </button>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--            </div>--}}
{{--        </div>--}}




    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
            text-style: bold;
        }
    </style>

    <br><br><div class="col-xl-03 col-lg-03 col-md-03 col-sm-01 col-12">
        <div class="card">
            <center>
                <div class="card-body">
                    <b> <h3 class="card-title"> Guest Personal Information </h3></b><br>
                    <div class="table-responsive">
                        <table style="width:75%">
                            <tr>
                                <th>Guest ID:</th>
                                <td>{{ $guest->guest_id }}</td>
                            </tr>
                            <tr>
                                <th>Guest Name:</th>
                                <td>{{ $guest->guest_name }}</td>
                            </tr>
                            <tr>
                                <th>Guest IC:</th>
                                <td>{{ $guest->guest_ic }}</td>
                            </tr>
                            <tr>
                                <th>Guest Phone Number:</th>
                                <td>{{ $guest->guest_pnum }}</td>
                            </tr>
                            <tr>
                                <th>Guest Email:</th>
                                <td>{{ $guest->guest_email }}</td>
                            </tr>
                        </table>
                        <br><br>
                        <a class= "btn btn-primary" href="{{ route('editguest', Auth::guard('guest')->user()->guest_id)}}">
                            <span>Update Profile</span> </a>


                        <form action ="{{ route ('guest.destroy', Auth::guard('guest')->user()->guest_id)}}" method="post">
                            @csrf
                            @method('DELETE')<br>
                            <button class ="btn btn-secondary" type="submit" padding: 10px; href="route ('guest.destroy', Auth::guard('guest')->user()->guest_id))">Delete</button>
                        </form>
                        </td>
                        </tr>
                        </form>

{{--                        <a class= "btn btn-secondary" href="">--}}
{{--                            <span>Delete Profile</span> </a>--}}
                    </div>
            </center>
        </div>
    </div>
    </div>
    </div>
@endsection
