@extends('layouts.adminmainguest')
@section('content')
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                <div class="">--}}
{{--                    <B><HR><BR> <CENTER> <h3> WELCOME GUEST!!</h3>--}}
{{--                            <h3> LAGUNA HOMESTAY BOOKING SYSTEM </h3></CENTER></B>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="container">
        <div class="col-xl-20 col-lg-18 col-md-22 col-sm-19 col-19">
            <div class="card-header1">
                <div class="row justify-content-center">
                    <div class="card-body1 ">
                        <div class="col-md">
                            <B><BR> <CENTER> <h3> Welcome Back {{Auth::guard('guest')->user()->guest_name}}<hr>
                                        {{--                                                Homestay Management System </h3></CENTER></B><hr><br>--}}
                                        <CENTER> <h3> </h3></CENTER>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-6 col-lg-8 col-md-8 col-sm-15 col-15">
                <div class="card">
                    <div class="card-body">

                        <h5 class="text-muted">Featured Unit</h5><hr>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-3">2 Storey Homestay</h1>

                            <a href="{{ route('homestay.view') }}">
                                <<img src="usertemp/img/homestay/signage/index 1.jpg" align="" width="400">
                            </a>
                        </div>
                        <hr>
                        <div class=" float-bottom text-success font-weight-bold">
                            <span><i class="fa fa-fw fa-arrow text-success"></i></span><span>2 Storey Unit with: </span><br>
                        </div>

                        <div class=" float-bottom  font-weight-regular">
                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 3 Bedroom</span>
                        </div>
                        <div class=" float-bottom  font-weight-regular">
                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 3 Bathroom</span>
                        </div>
{{--                        <div class=" float-bottom  font-weight-regular">--}}
{{--                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* Membership</span>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-lg-9 col-md-9 col-sm-15 col-15">
                <div class="card">
                    <div class="card-body">
                        <h5 class="text-muted">Featured Unit</h5><hr>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-3">Bungalow</h1>

                            <a href="{{ route('homestay.view') }}">
                                <img src="usertemp/img/homestay/signage/index 2.jpg" align="center" width="410">
                            </a>                                        </div>
                        <hr>
                        <div class=" float-bottom text-success font-weight-bold">
                            <span><i class="fa fa-fw fa-arrow text-success"></i></span><span>Bungalow Unit with: </span><br>
                        </div>
                        <div class=" float-bottom  font-weight-regular">
                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 5 Bedroom</span>
                        </div>
                        <div class=" float-bottom  font-weight-regular">
                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 4 Bathroom</span>
                        </div>
{{--                        <div class=" float-bottom  font-weight-regular">--}}
{{--                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* Membership</span>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>

        <br>
        <div class="col-xl-03 col-lg-03 col-md-03 col-sm-01 col-12">
            <div class="card">
                <center><h3 class="card-header">Booking List</h3></center><br>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <thead>
                            <tr>
                                <center>
                                    <th>Booking ID</th>
                                    <th>Homestay Image</th>
                                    <th>House Name</th>
                                    <th>House Type</th>
                                    <th>House Phone Number</th>
                                    <th>House Address</th>
                                    <th>Check In Date</th>
                                    <th>Check Out Date</th>
                                    <th>Booking Status</th>
{{--                                    <th>Booking Rate (RM)</th>--}}
                                    <th>Action</th>
                            </tr>
                            </thead>
                            @foreach($booking as $bookings)
                                <tr>
                                    <td>{{$bookings->booking_id}}</td>
                                    <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image }}" class="img-thumbnail" width="100" /></td>
                                    <td>{{$bookings->homestay->house_name}}</td>
                                    <td>{{$bookings->homestay->house_type}}</td>
                                    <td>{{$bookings->homestay->house_pnum}}</td>
                                    <td>{{$bookings->homestay->house_num}}</td>
                                    <td>{{$bookings->checkin_date}}</td>
                                    <td>{{$bookings->checkout_date}}</td>
                                    <td>{{$bookings->booking_status}}</td>
{{--                                    <td>{{$bookings->booking_rate}}</td>--}}
                                    <td>
                                            <a class= "btn btn-rounded btn-primary btn-md" href="{{route('booking.edit',$bookings->booking_id)}}" >View</a>
{{--                                            <a class= "btn btn-rounded btn-success" href="href" >View</a>--}}

                                        <form action ="#" method="post">
                                            @csrf
{{--                                            <ul><button class ="btn btn-rounded btn-secondary" type="submit" padding: 5px; href="route ('booking.destroy', $->house_id)">Delete</button></ul>--}}
                                        </form>
                                    </td>
                                </tr>
                                </form>
                                </a>
                                </td>
                                </tr>
                            @endforeach
                        </table><br>
                        {{ $booking->links()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection



