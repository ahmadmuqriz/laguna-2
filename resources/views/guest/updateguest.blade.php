@extends('layouts.adminmainguest')
@section('content')

    <br>
    <div class="container">
        <br><br>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <b><div class="card-header">{{ __('Profile Update') }}</div></b>

                    <div class="card-body">
                        <form class="cmxform form-horizontal style-form" id="signupForm" method="post" action="{{ route('updateguest', $guest->guest_id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="guest_name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="guest_name" type="text" class="form-control @error('guest_name') is-invalid @enderror" name="guest_name" value="{{ $guest->guest_name }}" required autocomplete="guest_name" autofocus>
                                    @error('guest_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="guest_ic" class="col-md-4 col-form-label text-md-right">{{ __('IC Number') }}</label>

                                <div class="col-md-6">
                                    <input id="guest_ic" type="text" class="form-control @error('guest_ic') is-invalid @enderror" name="guest_ic" value="{{ $guest->guest_ic }}" required autocomplete="guest_ic" autofocus>

                                    @error('guest_ic')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pnum" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                <div class="col-md-6">
                                    <input id="guest_pnum" type="text" class="form-control @error('guest_pnum') is-invalid @enderror" name="guest_pnum" value="{{ $guest->guest_pnum }}" required autocomplete="guest_pnum" autofocus>

                                    @error('guest_pnum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="guest_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="guest_email" type="email" class="form-control @error('guest_email') is-invalid @enderror" name="guest_email" value="{{ $guest->guest_email }}" required autocomplete="guest_email" autofocus>

                                    @error('guest_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
