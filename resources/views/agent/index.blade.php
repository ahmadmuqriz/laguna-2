@extends('layouts.adminmain')

@section('content')

    <style type="text/css">
        .zoomin img {
            height: 200px;
            width: 200px;
            -webkit-transition: all 2s ease;
            -moz-transition: all 2s ease;
            -ms-transition: all 2s ease;
            transition: all 2s ease;
        }
        .zoomin img:hover {
            width:310px;
            height:310px;
        }
    </style>
    <div class="container">
        <div class="col-xl-20 col-lg-18 col-md-22 col-sm-19 col-19">
            <div class="card-header1">
                <div class="row justify-content-center">
                    <div class="card-body">
                        <div class="col-md">
                            <B><BR> <CENTER> <h3> Welcome Back {{Auth::guard('agent')->user()->agent_name}}<hr>
{{--                                                Homestay Management System </h3></CENTER></B><hr><br>--}}
                                                    <CENTER> <h3> Unit To Be Added</h3></CENTER>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
            <div class="container-fluid dashboard-content">
            <!-- ============================================================== -->
            <!-- pageheader -->
            <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-15 col-15">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Upcoming Unit</h5><hr>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-3">English Unit</h1>

                                        <a href="{{route('homestay.create')}}">
                                        <<img src="usertemp/img/homestay/signage/index 1.jpg" align="center" width="400">
                                        </a>
                                    </div>
                                    <hr>
                                    <div class=" float-bottom text-success font-weight-bold">
                                        <span><i class="fa fa-fw fa-arrow text-success"></i></span><span>Single Storey Unit with: </span><br>
                                    </div>

                                    <div class=" float-bottom  font-weight-regular">
                                        <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 3 Bedroom</span>
                                    </div>
                                    <div class=" float-bottom  font-weight-regular">
                                        <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 3 Bathroom</span>
                                    </div>
                                    <div class=" float-bottom  font-weight-regular">
                                        <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* Membership</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-9 col-md-9 col-sm-15 col-15">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Upcoming Unit</h5><hr>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-3">Semi-Detachê</h1>

                                        <a href="{{route('homestay.create')}}">
                                            <img src="usertemp/img/homestay/signage/index 2.jpg" align="center" width="410">
                                        </a>                                        </div>
                                    <hr>

                                    <div class=" float-bottom text-success font-weight-bold">
                                        <span><i class="fa fa-fw fa-arrow text-success"></i></span><span>2 Storey Unit with: </span><br>
                                    </div>

                                        <div class=" float-bottom  font-weight-regular">
                                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 4 Bedroom</span>
                                        </div>
                                        <div class=" float-bottom  font-weight-regular">
                                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* 3 Bathroom</span>
                                        </div>
                                        <div class=" float-bottom  font-weight-regular">
                                            <span><i class="fa fa-fw fa-arrow text-danger"></i></span><span>* Membership</span>
                                        </div>
                                </div>
                        </div>
                    </div>
                     <br>

                    <center>
                    <div class="col-xl-20 col-lg-18 col-md-22 col-sm-19 col-19">
                        <div class="card">
                            <h2 class="card-header1" >Recent Booking</h2><br>
                            <div class="card-body p-1">
                                <div class="table table-striped table-bordered first">
                                    <table class="table">
                                        <thead class="bg-light">
                                        <tr class="border-0">
                                            <th>Booking ID</th>
                                            <th>Homestay Photos</th>
                                            <th>Guest Name</th>
                                            <th>Guest Phone Number</th>
                                            <th>House Name</th>
                                            <th>House Type</th>
                                            <th>Number Of Room</th>
{{--                                            <th>House Phone Number</th>--}}
                                            <th>Check In Date</th>
                                            <th>Check Out Date</th>
{{--                                            <th>Booking Status</th>--}}
{{--                                            <th>Booking Rate (RM)</th>--}}
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        @foreach($booking as $bookings)
                                            <tr>
                                                <td>{{$bookings->booking_id}}</td>
                                                <td><img src="{{ URL::to('/') }}/images/{{ $bookings->homestay->image }}" class="img-thumbnail" width="200" /></td>
                                                <td><center>{{$bookings->guest->guest_name}}</td>
                                                <td><center>{{$bookings->guest->guest_pnum}}</td>
                                                <td><center>{{$bookings->homestay->house_name}}</td>
                                                <td><center>{{$bookings->homestay->house_type}}</td>
                                                <td><center>{{$bookings->homestay->room_count}}</td>
{{--                                                <td><center>{{$bookings->homestay->house_pnum}}</td>--}}
                                                <td><center>{{$bookings->checkin_date}}</td>
                                                <td><center>{{$bookings->checkout_date}}</td>
{{--                                                <td><center>{{$bookings->booking_status}}</td>--}}
{{--                                                <td><center>{{$bookings->booking_rate}}</td>--}}
                                                <td>
                                                    <a class= "btn btn-rounded btn-primary btn-md" href="{{route('booking.detail', $bookings->booking_id)}}">View </a><br>

                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                   </center>
            </div>
    @endsection
