<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateHomestaysTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('homestays', function (Blueprint $table) {
            $table->bigIncrements('house_id')->index();
            $table->string('house_name');
            $table->string('house_type');
            $table->string('house_num');
            $table->string('house_own');
            $table->string('house_pnum');
            $table->string('room_count');
            $table->double('house_rate');
            $table->string('image');
            $table->string('image1');
            $table->string('image2');
            $table->string('image3');
            $table->bigInteger('agent_id')->unsigned();

            $table->foreign('agent_id')->references('agent_id')->on('agents');

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homestays');
    }
}
