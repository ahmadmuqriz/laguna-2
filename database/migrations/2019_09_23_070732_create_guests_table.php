<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->bigIncrements('guest_id')->index();
            $table->string('guest_name');
            $table->string('guest_ic');
            $table->string('guest_pnum');
            $table->string('guest_email')->unique();
            $table->string('guest_pass');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('guests');
        Schema::enableForeignKeyConstraints();
    }
}
