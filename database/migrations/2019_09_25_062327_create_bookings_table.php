<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Reference;
class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('booking_id')->index();
            //$table->dateTime('booking_date');
            $table->date('checkin_date');
            $table->date('checkout_date');
            $table->string('booking_status');

            $table->bigInteger('guest_id')->unsigned();
            $table->bigInteger('house_id')->unsigned();
            $table->timestamps();

            $table->foreign('guest_id')->references('guest_id')->on('guests');
            $table->foreign('house_id')->references('house_id')->on('homestays');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bookings');
        Schema::enableForeignKeyConstraints();

    }
}
